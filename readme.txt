This archive contains Python code and supporting files for the tasks of a Coursera MOOC. 

Course Title: Using Python to Access Web Data

Instructor: Charles Severance

Provider: University of Michigan

Summary: 
This course will show how one can treat the Internet as a source of data. We will scrape, parse, and read web data as well as access data using web APIs. We will work with HTML, XML, and JSON data formats in Python.

Link: https://www.coursera.org/learn/python-network-data/

Session Date: January 18th - March 7th 2016
