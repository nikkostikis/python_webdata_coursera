import urllib
# import xml.etree.ElementTree as ET
import json

serviceurl = raw_input('Enter an address - ')
# if len(serviceurl) < 1: serviceurl = 'http://python-data.dr-chuck.net/comments_42.json'
if len(serviceurl) < 1: serviceurl = 'http://python-data.dr-chuck.net/comments_231166.json'

print 'retrieving ', serviceurl

# load the data
info = json.loads(urllib.urlopen(serviceurl).read())

# keep the useful data in a list
lst = info['comments']
# print 'List: ', lst

# iterate and sum
# s = 0
# for dic in lst:
#    s = s + dic['count']

# print s

# use list comprehension to sum the values of all count elements in lst
print sum ( [ int(dic['count']) for dic in  lst ] )
