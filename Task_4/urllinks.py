# Note - this code must run in Python 2.x and you must download
# http://www.pythonlearn.com/code/BeautifulSoup.py
# Into the same folder as this program

import urllib
from BeautifulSoup import *
import re

url = 'https://pr4e.dr-chuck.com/tsugi/mod/python-data/data/known_by_Romy.html'

html = urllib.urlopen(url).read()
soup = BeautifulSoup(html)

# Retrieve all of the anchor tags
tags = soup('a')
# for tag in tags:
    # print tag.get('href', None)
names = []

for i in range(1,8):
    url2 = tags[17].get('href', None)
    names.append( re.findall('by_([a-zA-Z]+).', url2)[0].encode() )

    html2 = urllib.urlopen(url2).read()
    soup2 = BeautifulSoup(html2)
    # Retrieve all of the anchor tags
    tags = soup2('a')

# print the name that was pushed last - 7th name
print 'final name: ', names[6]
