# Note - this code must run in Python 2.x and you must download
# http://www.pythonlearn.com/code/BeautifulSoup.py
# Into the same folder as this program

import urllib
from BeautifulSoup import *
import re

# 1 or 2 for the default urls or anything else
inp = raw_input('Enter - ')
if inp == '1':
    url = 'http://python-data.dr-chuck.net/comments_42.html'
elif inp == '2':
    url = 'http://python-data.dr-chuck.net/comments_231165.html'
elif len(inp) == 0:
    print '''Please run me again, this time, having decided which
          url you would like me to read, otherwise you can hit 1 or 2
          for the defaults'''
else:
    url = inp
    print 'url: ', url

html = urllib.urlopen(url).read()

# create a soup object which encapsulates the html document, I'm guessing in
# trees and dictionaries
soup = BeautifulSoup(html)

# Retrieve all of the anchor tags
tags = soup('span')
summation = 0
for tag in tags:
    # Look at the parts of a tag
    print 'TAG:',tag
    print 'Contents:',tag.contents[0]
    contents = tag.contents[0]
    # I'll use a regular expression for the summation to avoid errors
    summation += sum ([int(x) for x in re.findall('[0-9]+', contents)])
print 'Summation: ', summation
