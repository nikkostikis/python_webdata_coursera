import urllib
import xml.etree.ElementTree as ET

serviceurl = raw_input('Enter an address - ')
if len(serviceurl) < 1: serviceurl = 'http://python-data.dr-chuck.net/comments_231162.xml'

print 'retrieving ', serviceurl

# construct a list with all count elements from the url, which will
# be read as a string and parsed by the ET
lst = ET.fromstring(urllib.urlopen(serviceurl).read()).findall('.//count')

# use list comprehension to sum the texts of all count elements in lst
print sum ( [ int(item.text) for item in  lst ] )
